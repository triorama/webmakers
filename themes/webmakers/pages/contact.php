<?php
/**
 * Template Name: Contact
 *
 * The template for displaying pages with sidebar.
 *
* @package webmakers
 */

?>

<section id="contact" class="contact-section fill">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<?php dynamic_sidebar( 'footer-1' ); ?>
			</div>
			<div class="col-md-4">
				<?php dynamic_sidebar( 'footer-2' ); ?>
			</div>
			<div class="col-md-4">
				<?php dynamic_sidebar( 'footer-3' ); ?>
			</div>
		</div>
	</div>
</section>
