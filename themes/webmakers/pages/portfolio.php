<?php
/**
 * Template Name: Portfolio
 *
 * The template for displaying pages with sidebar.
 *
 * @package webmakers
 */

?>

<section id="section-portfolio" class="portfolio-section">
	<div class="container">
		<?php $the_query = new WP_Query( array( 'pagename' => 'clientes' ) ); ?>
				
		<?php while ( $the_query -> have_posts() ) : $the_query -> the_post();  ?>

			<h1><?php the_field( 'highlight' ); ?></h1>
			<h2 class="subtitles highlight"><?php the_field( 'top_zero' ); ?></h2>
			<div class="clients-tabs" style="margin-top: 20px;">

				<?php $clients = get_terms( 'portfolio_categories' ); ?>
			 
				<!-- Nav tabs -->
				<ul class="nav nav-tabs nav-justified">
					<li class="active">
						<a data-toggle="tab" href="#all">Todas</a>
					</li>
					<?php foreach ( $clients as $client ) { ?>
						<li>
							<a href="#<?php echo $client->slug ?>" data-toggle="tab">
								<?php echo $client->name ?>
							</a>
						</li>
					<?php } ?>
				</ul>
			 
				<!-- Tab panes -->
				<div class="tab-content">
			 
					<div class="tab-pane active" id="all">
						<?php
						$args = array(
							'post_type' => 'portfolio',
							'posts_per_page' => 5,
							'orderby' => 'title',
							'order' => 'ASC',
						);
						$all_clients = new WP_Query( $args );
						?>
			 
						<div class="table-responsive">
							<table class="table">
								<?php while ( $all_clients->have_posts() ) : $all_clients->the_post(); ?>

								<tr>
									<td><?php the_post_thumbnail() ?></td>
									<td><h3><?php the_title() ?></h3></td>
									<td>
										<p class="lead"><?php the_excerpt() ?></p>
										<a href="<?php the_permalink() ?>" class="btn btn-success">Leia mais</a>
									</td>
								</tr>
								<?php endwhile; ?>
								<?php wp_reset_postdata() ?>
							</table>
						</div>
			 
					</div><!-- all clients tab pane -->
			 
					<?php foreach ( $clients as $client ) { ?>
			 
						<div class="tab-pane" id="<?php echo $client->slug ?>">
							<?php
							$args = array(
								'post_type' => 'portfolio',
								'posts_per_page' => 5,
								'orderby' => 'title',
								'order' => 'ASC',
								'tax_query' => array(
									array(
										'taxonomy' => 'portfolio_categories',
										'field' => 'slug',
										'terms' => $client->slug,
									),
								),
							);
							$clients = new WP_Query( $args );
							?>
							<div class="table-responsive">
								<table class="table">
									<?php while ( $clients->have_posts() ) : $clients->the_post(); ?>	
									<tr>
										<td><?php the_post_thumbnail() ?></td>
										<td><h3><?php the_title() ?></h3></td>
										<td>
											<p class="lead"><?php the_excerpt() ?></p>
											<a href="<?php the_permalink() ?>" class="btn btn-success">Leia mais</a>
										</td>
									</tr>
									<?php endwhile; ?>
									<?php wp_reset_postdata() ?>
								</table>
							</div>
						</div>
					<?php }  ?>
			 
				</div><!-- tab-content -->
			 
			</div><!-- clients-tabs -->
		<?php endwhile;?>
	</div>
</section>
