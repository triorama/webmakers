<?php
/**
 * Template Name: Intro
 *
 * The template for displaying pages with sidebar.
 *
* @package webmakers
 */

?>

<section id="intro" class="intro-section section-gray fill">
	<div class="flexslider">
	  <ul class="slides">
		<div class="bg"></div>

		<?php
		$exec_query = new WP_Query( array( 'post_type' => 'slide', 'posts_per_page' => -1 ) );
		if ( $exec_query->have_posts() ) {
		?>

		<?php while ( $exec_query->have_posts() ) : $exec_query->the_post(); ?>

			<li style="background-image: url('<?php if ( get_field( 'slide_image' ) ) { $bg_slide = the_field( 'slide_image' ); } ?>'); background-position: top">
				<div class="flex-caption container">
					<h2><?php the_title();?></h2>
					<h3><?php the_field( 'subtitulo' ); ?></h3>
				</div>
			</li>

		<?php endwhile; ?>

		<?php wp_reset_postdata(); } ?>

	</ul>
	</div>
</section>