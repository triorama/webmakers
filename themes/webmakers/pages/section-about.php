<?php
/**
 * Template Name: Section About
 *
 * The template for displaying page About Me.
 *
 * @package webmakers
 */

?>

<section id="section-about" class="section-gray">
	<div class="container">
		<?php $the_query = new WP_Query( array( 'pagename' => 'about' ) ); ?>
				
		<?php while ( $the_query -> have_posts() ) : $the_query -> the_post();  ?>
				
			<h1><?php the_field( 'highlight' ); ?></h1>
			<h2 class="subtitles highlight"><?php the_field( 'top_zero' ); ?></h2>
			
				<div class="about-img col-md-6">
					<?php if ( has_post_thumbnail() ) {
						the_post_thumbnail();
					} else { ?>
					<img src="https://placehold.it/180x180" class="img-responsive" alt="">
					<!--<img src="<?php bloginfo('template_directory'); ?>/assets/images/no-image.jpg" alt="<?php the_title(); ?>" />-->
					<?php } ?>
				</div>
				<div class="services col-md-6">
					
					<?php the_content(); ?>

					<?php

					$exec_query = new WP_Query( array (
					  'post_type' => 'service',
						'posts_per_page' => -1,
						'order' => 'ASC',
					) );

					if ( $exec_query->have_posts() ) { ?>

					<?php while ( $exec_query->have_posts() ): $exec_query->the_post(); ?>

						<div class="col-md-4">
							<p><?php the_field( 'icon' ) ?></p>
							<h4><?php the_title();?></h4>
							<p><?php the_field( 'description' ) ?></p>
						</div>

					<?php endwhile; ?>

					<?php wp_reset_postdata(); } ?>
					
				</div>

		<?php endwhile;?>
	</div>
</section>
