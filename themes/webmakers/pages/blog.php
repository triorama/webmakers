<?php
/**
 * Template Name: Blog
 *
 * The template for displaying pages with sidebar.
 *
* @package webmakers
 */

?>

<section id="section-blog" class="blog-section">
	<div class="container">
		<?php $the_query = new WP_Query( array( 'pagename' => 'contact' ) ); ?>
				
		<?php while ( $the_query -> have_posts() ) : $the_query -> the_post();  ?>
				
				<h1><?php the_field( 'highlight' ); ?></h1>
				<h2 class="subtitles highlight"><?php the_field( 'top_zero' ); ?></h2>
				
				<div class="row">
					<div class="col-md-6">
						
						<?php the_content(); ?>
						
					</div>

					<div class="about-img col-md-6">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1842.9438080508928!2d-48.50081266032331!3d-27.594148452280184!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9527391f9dc47fa5%3A0x3f265c0398fac970!2sR.+Itabira%2C+232+-+Itacorubi%2C+Florian%C3%B3polis+-+SC!5e0!3m2!1spt-BR!2sbr!4v1506967371589" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>

		<?php endwhile;?>
	</div>
</section>
