<?php
/**
 * Plugin Name: Web Makers Base
 * Description: Define os post types, taxonomias e configurações.
 * Version: 1.0
 * Author: marioernestoms
 * Author URI: http://marioernestoms.com
 *
 * @package web_makers_base
 */

add_action( 'init', 'register_cpt_slide' );

function register_cpt_slide() {

	$labels = array(
		'name' => _x( 'Slides', 'slide' ),
		'singular_name' 	 => _x( 'Slides', 'slide' ),
		'add_new' 			 => _x( 'Add New', 'slide' ),
		'add_new_item' 		 => _x( 'Add New Slides', 'slide' ),
		'edit_item' 		 => _x( 'Edit Slides', 'slide' ),
		'new_item' 			 => _x( 'New Slides', 'slide' ),
		'view_item' 		 => _x( 'View Slides', 'slide' ),
		'search_items' 		 => _x( 'Search Slides', 'slide' ),
		'not_found' 		 => _x( 'No projects found', 'slide' ),
		'not_found_in_trash' => _x( 'No projects found in Trash', 'slide' ),
		'parent_item_colon'  => _x( 'Parent Slides:', 'slide' ),
		'menu_name' 		 => _x( 'Slides', 'slide' ),
	);

	$args = array(
		'labels' => $labels,
		'hierarchical' 	=> false,
		'description'  	=> 'Things I\'ve worked on or towards',
		'supports' 		=> array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields' ),

		'public' 		=> true,
		'show_ui' 		=> true,
		'show_in_menu' 	=> true,
		'menu_position' => 30,
		'menu_icon' 	=> 'dashicons-format-gallery',

		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => array(
			'slug' => 'slide',
		),
		'capability_type' => 'post',
	);

	register_post_type( 'slide', $args );
}

function slide_cpt_archive_order( $vars ) {
	if ( !is_admin() && isset( $vars['post_type'] ) ) {
		if ( $vars['post_type'] == 'slide' ) {
			$vars['orderby'] = 'title';
			$vars['order'] = 'ASC';
		}
	}

	return $vars;
}
add_filter( 'request', 'slide_cpt_archive_order' );


add_action( 'init', 'register_cpt_services' );

function register_cpt_services() {

	$labels = array(
		'name' 				 => _x( 'Serviços', 'serviço' ),
		'singular_name' 	 => _x( 'Serviços', 'serviço' ),
		'add_new' 			 => _x( 'Adicionar novo', 'serviço' ),
		'add_new_item' 		 => _x( 'Adicionar novo Serviços', 'serviço' ),
		'edit_item' 		 => _x( 'Editar Serviços', 'serviço' ),
		'new_item' 			 => _x( 'Novo Serviços', 'serviço' ),
		'view_item' 		 => _x( 'Ver Serviços', 'serviço' ),
		'search_items'       => _x( 'Buscar Serviços', 'serviço' ),
		'not_found' 		 => _x( 'No projects found', 'serviço' ),
		'not_found_in_trash' => _x( 'No projects found in Trash', 'serviço' ),
		'parent_item_colon'  => _x( 'Parent Serviços:', 'serviço' ),
		'menu_name' 		 => _x( 'Serviços', 'serviço' ),
	);

	$args = array(
		'labels' 		=> $labels,
		'hierarchical' 	=> false,
		'description' 	=> 'Things I\'ve worked on or towards',
		'supports' 		=> array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields' ),

		'public' 		=> true,
		'show_ui' 		=> true,
		'show_in_menu' 	=> true,
		'menu_position' => 30,
		'menu_icon' 	=> 'dashicons-clipboard',

		'show_in_nav_menus' 	=> true,
		'publicly_queryable' 	=> true,
		'exclude_from_search' 	=> false,
		'has_archive' 			=> true,
		'query_var' 			=> true,
		'can_export' 			=> true,
		'rewrite' 	=> array(
			'slug'	=> 'service',
		),
		'capability_type' 		=> 'post',
	);

	register_post_type( 'service', $args );
}

function service_cpt_archive_order( $vars ) {
	if ( !is_admin() && isset( $vars['post_type'] ) ) {
		if ( $vars['post_type'] == 'service' ) {
			$vars['orderby'] = 'title';
			$vars['order'] = 'ASC';
		}
	}

	return $vars;
}
add_filter( 'request', 'service_cpt_archive_order' );


function portfolio_register() {
	$labels = array(
		'name' => _x( 'Clientes', 'post type general name' ),
		'singular_name' 	 => _x( 'Cliente Item', 'post type singular name' ),
		'add_new' 			 => _x( 'Adicionar novo', 'portfolio item' ),
		'add_new_item' 		 => __( 'Adicionar novo item de Cliente' ),
		'edit_item' 		 => __( 'Editar Item de Cliente' ),
		'new_item' 			 => __( 'Novo item de Cliente' ),
		'view_item' 		 => __( 'Ver item de Cliente' ),
		'search_items' 		 => __( 'Buscar Itens de Cliente' ),
		'not_found' 		 => __( 'Nothing found' ),
		'not_found_in_trash' => __( 'Nothing found in Trash' ),
		'parent_item_colon'	 => '',
	);
	$args = array(
		'labels' 				=> $labels,
		'public' 				=> true,
		'publicly_queryable' 	=> true,
		'show_ui' 				=> true,
		'query_var' 			=> true,
		'rewrite' 				=> true,
		'capability_type' 		=> 'post',
		'hierarchical' 			=> false,
		'menu_position' 		=> 30,
		'menu_icon' 			=> 'dashicons-portfolio',
		'supports' 				=> array( 'title','editor','thumbnail' ),
	);
	register_post_type( 'portfolio' , $args );
}
add_action( 'init', 'portfolio_register' );

/**
 * Taxonomies for Portfolio Section.
 */
function create_portfolio_taxonomies() {
	$labels = array(
		'name'              => _x( 'Cidades', 'taxonomy general name' ),
		'singular_name'     => _x( 'Cidade', 'taxonomy singular name' ),
		'search_items'      => __( 'Buscar Cidades' ),
		'all_items'         => __( 'Todas as Cidades' ),
		'parent_item'       => __( 'Cidade Mãe' ),
		'parent_item_colon' => __( 'Cidade Mãe:' ),
		'edit_item'         => __( 'Editar Cidade' ),
		'update_item'       => __( 'Update Cidade' ),
		'add_new_item'      => __( 'Adiconar nova Cidade' ),
		'new_item_name'     => __( 'Novo nome de Cidade' ),
		'menu_name'         => __( 'Cidades' ),
	);

	$args = array(
		'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags).
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'categories' ),
	);

	register_taxonomy( 'portfolio_categories', array( 'portfolio' ), $args );
}
add_action( 'init', 'create_portfolio_taxonomies', 0 );